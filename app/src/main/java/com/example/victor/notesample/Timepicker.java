package com.example.victor.notesample;

/**
 * Created by витя on 07.05.2017.
 */

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Timepicker extends AppCompatActivity {
    private static final int NOTIFY_ID = 101;
    public ArrayList<Date> TaskTime;
    private TextView mInfoTextView;
    private android.widget.TimePicker mTimePicker;
    private  NoteAdapter adapter;
    private int j,Taskhour,Taskmin;


    Date date = new Date();
    Date date1 = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");
    Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.time_picker);
        mInfoTextView = (TextView) findViewById(R.id.textView);
        mTimePicker = (android.widget.TimePicker) findViewById(R.id.timePicker);



        Calendar now = Calendar.getInstance();

        mTimePicker.setCurrentHour(now.get(Calendar.HOUR_OF_DAY));
        mTimePicker.setCurrentMinute(now.get(Calendar.MINUTE));

        mTimePicker.setOnTimeChangedListener(new android.widget.TimePicker.OnTimeChangedListener() {

            @Override
            public void onTimeChanged(android.widget.TimePicker view, int hourOfDay, int minute) {
                Toast.makeText(getApplicationContext(), "onTimeChanged",
                        Toast.LENGTH_SHORT).show();
                Taskhour=hourOfDay;
                Taskmin=minute;
                adapter.setTaskTime(Taskhour,Taskmin);
                Date taskTime = new Date();
                taskTime.setHours(hourOfDay);
                taskTime.setMinutes(minute);
                TaskTime.add(taskTime);
                j++;
                mInfoTextView.setText("Часы: " + hourOfDay + "\n" + "Минуты: "
                        + minute);
            }
        });
        for (int i = 0; i < j; i++) {
            if (simpleDateFormat.format(date) == simpleDateFormat.format(TaskTime.get(i + 1))) {
              CreateNotify();
            }
        }

    }
    private void CreateNotify() {

        Context context = getApplicationContext();
        Intent notificationIntent = new Intent(context, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
// оставим только самое необходимое
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.drawable.ic_notify)
                .setContentTitle("Напоминание")
                .setContentText("Задача скоро завершится!");

        Notification notification = builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        notificationManager.notify(NOTIFY_ID, notification);
    }







    public void monChecked(int position){
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        date1=TaskTime.get(position + 1);
        if ((simpleDateFormat.format(date) == simpleDateFormat.format(date1)) &(dayOfWeek==2)){
           CreateNotify();

        }
    }

    public void tueChecked(int position){
        date1=TaskTime.get(position + 1);
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        if ((simpleDateFormat.format(date) == simpleDateFormat.format(date1)) &(dayOfWeek==3)){
            CreateNotify();

        }
    }


    public void wenChecked(int position){
        c.setTime(date);
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
        date1=TaskTime.get(position + 1);
        if ((simpleDateFormat.format(date) == simpleDateFormat.format(date1)) &(dayOfWeek==4)){
            CreateNotify();

        }
    }

}

