package com.example.victor.notesample;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import javax.crypto.spec.DHGenParameterSpec;

public class MainActivity extends AppCompatActivity {


    private RecyclerView mRVNotes;
    private NoteAdapter mAdapter;
    private FloatingActionButton mBAddNote;
    private EditText mETTime;


    private DBHelper dbHelper = new DBHelper(this);

    //Intent intent = new Intent(this,EditorActivity.class);
    //Intent intent1 = new Intent(this,MainActivity.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mETTime = (EditText) findViewById(R.id.tv_note_time);
        mRVNotes = (RecyclerView) findViewById(R.id.rv_notes);
        mRVNotes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mAdapter = new NoteAdapter(this);
        dbHelper = new DBHelper(this);
        ArrayList<Note> notes = dbHelper.getAllNotes();
         /*notes.add(new Note("Заметка", 1384539545354L));
         notes.add(new Note("Рецепт", 1584539545354L));
         notes.add(new Note("Стишок", 1599539545354L));
         notes.add(new Note("Не забыть", 1684539545354L));
         notes.add(new Note("Список покупок", 1784539545354L));
         notes.add(new Note("Важно!", 1884539545354L));
         notes.add(new Note("Нужные документы", 1984539545354L));
         notes.add(new Note("Забрать долг", 2084539545354L));
 */

        mAdapter.setmNotes(notes);
        mRVNotes.setAdapter(mAdapter);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRVNotes.getContext(),
                DividerItemDecoration.VERTICAL);
        mRVNotes.addItemDecoration(dividerItemDecoration);

        mBAddNote = (FloatingActionButton) findViewById(R.id.b_add_note);
        mBAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                intent.putExtra(EditorActivity.INTENT_KEY_NOTE_ID, -1);
                MainActivity.this.startActivity(intent);
            }
        });


    }
    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            setTitle("Заметки");
        }
    }





}
