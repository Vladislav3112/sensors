package com.example.victor.notesample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;

public class EditorActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String INTENT_KEY_NOTE_ID = "note_id";
    private DBHelper mDBHelper;
    private Note mNote;
    public CheckBox CBmon,CBtue,CBwen,CBthur,CBfri,CBsut,CBsun;
    private EditText mETTitle, mETDescription;
    private TextView mTVDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        mTVDate = (TextView) findViewById(R.id.tv_date);
        mETDescription = (EditText) findViewById(R.id.tv_note_text);
        mETTitle = (EditText) findViewById(R.id.tv_title);

        CBmon = (CheckBox) findViewById(R.id.ch_box_1_st_day);
        CBtue = (CheckBox) findViewById(R.id.ch_box_2_nd_day);
        CBwen = (CheckBox) findViewById(R.id.ch_box_3_rd_day);
        CBthur = (CheckBox) findViewById(R.id.ch_box_4_th_day);
        CBfri = (CheckBox) findViewById(R.id.ch_box_5_th_day);
        CBsut = (CheckBox) findViewById(R.id.ch_box_6_th_day);
        CBsun = (CheckBox) findViewById(R.id.ch_box_7_th_day);

        mDBHelper = new DBHelper(this);
        int id = getIntent().getIntExtra(INTENT_KEY_NOTE_ID, -1);


    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle("Новая заметка");
            setTitle("Новая заметка");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.tv_task_time) {
            Intent intent = new Intent(EditorActivity.this, Timepicker.class);
            startActivity(intent);
        }
        if (view.getId()==R.id.b_add_note){
            String title = mETTitle.getText().toString();

            Date date = new Date();
            Note note = new Note(title,date.getTime());
            mDBHelper.createNote(note);

            Intent intent = new Intent(EditorActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
